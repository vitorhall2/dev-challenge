import React, { Component } from 'react';
import '../static/styles/scss/menu-header.scss';
import {Link} from 'react-router-dom';
import Categories from './Categories';

class Header extends Component {
  constructor(props){
    super(props)
    
  }


  render(){
    return(
      <nav  className={this.props.toggle == true? 'mouseEnter menu-drop': 'mouseLeave menu-drop'}>          
        <div className="container-inner-menu">
          <div className="col-md-4 display-box-list">
            <h4>Categorias</h4>
            <ul className="lista-cat display-list">
              <li ><Link to='/categorias'>VESTIDO</Link></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
            </ul>
            <ul className="lista-cat display-list alinhamento-colun">
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              
            </ul>
          </div>
          <div className="col-md-4 display-box-list">
            <h4>Acessórios</h4>
            <ul className="lista-cat display-list">
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
            </ul>
            <ul className="lista-cat display-list alinhamento-colun">
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              
            </ul>
          </div>
          <div className="col-md-4 display-box-list">
            <h4>Intimates</h4>
            <ul className="lista-cat display-list">
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
            </ul>
            <ul className="lista-cat display-list alinhamento-colun">
              <li ><a href="#">VESTIDO</a></li>
              <li ><a href="#">VESTIDO</a></li>
              
            </ul>
          </div>
          
        </div>
      </nav>
    )
  }
}
export default Header;
