import React, { Component } from 'react';
import '../static/styles/scss/header.scss';
import lojas from '../static/img/lojas.png';
import contato from '../static/img/contato.png';
import login from '../static/img/login.png';
import sacola from '../static/img/sacola.png';
import inside from '../static/img/inside.png';
import MenuHeader from './Menu-header'
import { Link } from 'react-router-dom';

class Header extends Component {
  constructor(props){
    super(props)
    this.state = {
      dropdownOpen1: false,
    };
    this.onMouseEnter = this.onMouseEnter.bind(this)
  }

  onMouseEnter(){
    if(this.state.dropdownOpen1 == false){
      this.setState({dropdownOpen1: true })
    }
  }

  onMouseLeave= ()=> {
    if(this.state.dropdownOpen1 == true){
      this.setState({dropdownOpen1: false })
    }
  }

  render(){
    return(
      <header className="container-header header-backgound">
        <div className="text-center box-logo"><Link className="logo" to={'/'}>ANIMALE</Link></div>
        <nav className="nav">
          <div className="menu-servico ">
            <ul>
              <li><a href="#"><img title="Lojas" src={lojas}/></a></li>
              <li><a href="#"><img title="Contato" src={contato}/></a></li>
            </ul>
          </div>    
          <div className="menu-loja text-center">
            <ul className="">
              <li><a href="#">NOVIDADES</a></li>
              <li
                onMouseOver={this.onMouseEnter}
                onMouseLeave={this.onMouseLeave}
                isOpen={this.state.dropdownOpen1}>
                <a href="#">
                  COLEÇÃO
                </a>
                <MenuHeader toggle={this.state.dropdownOpen1}/>
              </li>
              
              <li><a href="#">JOIAS</a></li>
              <li><a href="#">SALE</a></li>
              <li><a href="#"><img src={inside}/></a></li>
            </ul>
          </div>    
          <div className="menu-usuario">
            <ul className="text-right">
              <li><a href="#"><img title="Lojas" src={login}/></a></li>
              <li><a href="#"><img title="Contato" src={sacola}/></a></li>
            </ul>
          </div>
        </nav>
       
      </header>
    )
  }
}
export default Header;
