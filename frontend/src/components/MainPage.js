import React, { Component } from 'react';
import '../static/styles/scss/main-page.scss';
import banner from '../static/img/captura.jpg'
import banner2 from '../static/img/cap2.png'

class MainPage extends Component {
  
  
  render(){
    return(
      <div className="">
        <div className="banner-main">
          <img className="img-fluid" src={banner}></img>
        </div>
        <div className="second-banner-main">
          <div className="box-text-second-banner">
            <h3>GO MODERN</h3>
            <p>Nada óbvios, os shapes são contemporâneos com recortes estratégicos e estampas abstratas.</p>
            <a href="#">SHOP NOW</a>
          </div>
          <img className="img-fluid box-50-right" src={banner2}/>
        </div>
      </div>
    )
  }
}
export default MainPage;
