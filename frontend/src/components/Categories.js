import React, { Component } from 'react';
import Axios from 'axios';
import { URLS } from '../constants/constants';
import '../static/styles/scss/cotegories.scss';
import bannerCat from '../static/img/ban-categori.jpg'
import imgProduto from '../static/img/teste_IMG.png'

class Categories extends Component {
  constructor(props){
    super(props)
    this.state = {
      produtos:[]
    }
    this.obterListaProdutos = this.obterListaProdutos.bind(this)
  }
  componentDidMount(){
    this.obterListaProdutos()
  }

  obterListaProdutos(){
    Axios.get(URLS.TESTE_JSON)
    .then(res =>{
        if(res){
          const produtos = res.data;
          this.setState({produtos})
        }
        //console.log(res.data);
    }).catch(err => {
        console.log(err);
    })
  }
  

  
  render(){
    const produtos = this.state.produtos
    return(
      
      <div className="">
        <div className="banner-main">
          <img className="img-fluid" src={bannerCat}></img>
        </div>
        <div className="box-filtro">
          <div className="col-md-3">

            <div className="breadcrumb-cat"><p>Coleção > Couro</p></div>
          </div>
          <div className="col-md-6">
            <div className="filtros">

            </div>
          </div>
          <div className="col-md-3"></div>
          <div className="lista-produtos row">
            {console.log(produtos)}
            {produtos.map((produto, ) =>
            <React.Fragment>
              <div className="box-produto col-md-4">
                <div className="box-image">
                  <img className="img-fluid" src={produto.link}/>
                </div>
                <div className="box-info-produto">
                  <div className="nome-produto">{produto.productName}</div>
                  <div className="valor-produto row">
                    <div className="valor-total col-md-4">  R$ 1.198,00</div>
                    <div className="valor-parcelado col-md-8">10x de R$ 119,90</div>
                  </div>
                  <div className="btn-produto">
                    <a href="">QUICK SHOP</a>
                  </div>
                  
                </div>
              </div>
            </React.Fragment>
            )}
          </div>
        </div>
      </div>
    )
  }
}
export default Categories;
