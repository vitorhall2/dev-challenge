import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'
import './static/styles/css/App.css'
import Header from '../src/components/Header'
import MainPage from '../src/components/MainPage'
import Categories from '../src/components/Categories'

class App extends Component {
  constructor(props){
    super(props)
  }

  
  render(){
    return(
      <React.Fragment>
      <Header/>
      <Switch>
        <div className="main">
          <Route path="/" exact={true} component={MainPage} />
          <Route path="/categorias" component={Categories} />
        </div>
      </Switch>
      </React.Fragment>
    )
  }
}
export default App;
